from resource import *
import json
import time
import requests
import os
import subprocess
import sys
import grpc
sys.path.append("./service_spec")
sys.path.append("./service/service_spec")

import nunet_adapter_pb2
import nunet_adapter_pb2_grpc

try:
    deployment_type=os.environ['deployment_type']
except:
    deployment_type=""

class resutils():
    def __init__(self):
        self.device_name="news_score"


    def memory_usage(self):
        to_MB = 1024.
        to_MB *= to_MB
        return getrusage(RUSAGE_SELF).ru_maxrss / to_MB

    def cpu_ticks(self):
        sec= time.clock()
        tick=sec*(10**7)
        mtick=tick/10**6
        return mtick

    def block_in(self):
        to_KB = 1024.
        return getrusage(RUSAGE_SELF).ru_inblock/to_KB

    def get_address(self):   
        #get deployment type to identify the static port
        deployment_type=os.environ['deployment_type']       
        service_address="localhost"
        if deployment_type=="prod":    
            service_port=60777
        else:
            service_port=60778
        return str(service_address),str(service_port)    

    async def call_telemetry(self,fn_score,cpu_used,memory_used,net_used,time_taken,nunet_adapter_address,call_id,tracer_info):
        channel = grpc.aio.insecure_channel("{}".format(nunet_adapter_address))
        stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
        span_id = tracer_info['span_id']
        trace_id = tracer_info['trace_id']
        trace_info = str({
            'trace_id': trace_id,
            'span_id': span_id
        })
        if deployment_type=="prod":    
            service_name="news-score"
        else:
            service_name="testing-news-score"
        result=await stub.telemetry(nunet_adapter_pb2.TelemetryInput(result=fn_score,cpu_used=cpu_used,memory_used=memory_used,net_used=net_used,time_taken=time_taken,device_name=self.device_name,call_id=call_id,service_name=service_name,tracer_info=trace_info))
        return str(result.response)

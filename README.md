# FakeNews Score Snet service 

Fake News score is an AI service that orchestrates all other services constituting to fake news warning application. It implements an ensemble function which combines weighted results of other services and outputs the general probability estimation (in %) that an article contains fake news. News score is extendable and can include other services (provided by community or third party developers) into an ensemble if they contribute to higher quality of the application.

Snet Marketplace service for FakeNews stance detection scoring [Athene FNC-1 Submission](https://github.com/hanselowski/athene_system) and [UCLNLP FNC-1 Submission](https://mr.cs.ucl.ac.uk/)

## Setup


	docker build -t fns_snet .
	
	# map daemon and service ports
	docker run -p 7002:7002 -p 7009:7009 \ 
	-e nunet_adapter_name=$nunet_adapter_name \ 
	-e nunet_adapter_uclnlp_name=$nunet_adapter_uclnlp_name \
	-e nunet_adapter_athene_name=$nunet_adapter_athene_name \
	-it fns_snet bash

	# snet request to service (using snet or the test script)
	snet client call nunet-org-new fake-news-score-service default_group fn_score_calc '{"headline":"news_headline","body":"news_body"}' 
	
	python3 test_fake_news_score.py

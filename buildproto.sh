python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service/service_spec/nunet_adapter.proto

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service/service_spec/fake_news_score.proto

python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. service/service_spec/service_proto.proto

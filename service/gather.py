import json
import logging
import asyncio
import threading
import os
from asyncio import get_event_loop, ensure_future

from resutils import *
import service.service_spec.nunet_adapter_pb2 as pb2
import service.service_spec.nunet_adapter_pb2_grpc as pb2_grpc


try:
    deployment_type=os.environ['deployment_type']
except:
    deployment_type=""


utils=resutils()

get_address=utils.get_address()
address=get_address[0]
port=get_address[1]

#get adapter#1 address
nunet_adapter_address=address+":"+port

metadata=json.load(open("service/dag.json", "r"))
service_definitions=[]


if deployment_type=="prod":
    metadata["dag"]={
        "news-score" : ["uclnlp", "binary-classification"]
    }
else:
    metadata["dag"]={
        "testing-news-score" : ["testing-uclnlp", "testing-binary-classification"]
    }
    
dag=metadata["dag"]
logging.debug(dag)

# until all services running on the platform can be requested via rest-api 
# the mapping of service-name to adapter name is hardcoded here;
# however in  principle adapter names (as well as addresses) should be resolved by nunet-adaper utils.py calling rest-api...

#TODO remove adapter name parameter from the whole platform
adapters={"testing-uclnlp":"testing-nunet-adapter-uclnlp",
          "testing-news-score":"testing-nunet-adapter-news-score",
          "testing-binary-classification":"testing-nunet-adapter-nunet-demo"
        }

channel = grpc.aio.insecure_channel("{}".format(nunet_adapter_address))

async def gather_metadata():
    #get metadata of services using adapter#1 >> adapter#2/#3 >> services
    for k in list(dag.keys()):
        for service in dag[k]:
            stub = pb2_grpc.NunetAdapterStub(channel)
            fp = pb2.metaAdapterParams()
            fp.service_name = service
            
            result = await stub.reqAdapterMetadata(fp)

            # result = next(result)
            metadata=result.service_definition
            

            service_definitions.append(json.loads(metadata))

            i=dag[k].index(service)
            dag[k][i]=eval(metadata)
    keys=list(dag.keys())
    i=len(keys)-1

    #set metadata of services starting from the last dag element. e.g athene_system[services]=metadaof(nlp_model)
    while i>=0:
        services=[]
        for service in dag[keys[i]]:
            services.append(service)
        if i!=0:
            for key in keys:
                for service in  dag[key]:                    
                    if keys[i] == service["name"]:
                        service["services"]=services
        i-=1

    #get the orchestrator metadata
    if deployment_type=="test":
        orch_metadata=json.load(open("service/service_spec/service_definition.json", "r"))
    else:
        orch_metadata=json.load(open("service/service_spec/service_definition_prod.json", "r"))

    if deployment_type=="prod":
        #get the orchestrator services from the dag
        orch_metadata["name"]="news-score"
        orch_metadata["services"]=dag["news-score"]
        service_definitions.append(orch_metadata)
        metadata=json.load(open("service/dag_prod.json", "r"))

    else:
        orch_metadata["name"]="testing-news-score"
        orch_metadata["services"]=dag["testing-news-score"]
        service_definitions.append(orch_metadata)
        metadata=json.load(open("service/dag.json", "r"))

    #set all metadata to fake-news-detection news
    metadata["services"]=orch_metadata

    #write to a file with name metadata.json
    with open('metadata.json', 'w') as metadata_json:
        json.dump(metadata, metadata_json, indent=4)
    

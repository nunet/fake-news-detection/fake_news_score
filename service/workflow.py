import sys
import grpc
import time
import os
import asyncio


import logging
import log

from opentelemetry.trace import NonRecordingSpan, SpanContext, TraceFlags
from opentelemetry import trace
import service.service_spec.nunet_adapter_pb2 as pb2
import service.service_spec.nunet_adapter_pb2_grpc as pb2_grpc


from run_fake_news_score import Log
import json

from resutils import *

import service.gather as gather
import concurrent.futures

import ast

from time import sleep

import threading

logger=Log.logger


try:
    deployment_type=os.environ['deployment_type']
except:
    deployment_type=""

try:
    tokenomics_mode=os.environ['tokenomics_mode']
except:
    tokenomics_mode=""


class workflow():
    def __init__(self):
        self.utils=resutils()

    async def verify(self,channel,result,txn_hash,escrow_address,public_key,amount):
        amount/=10**4
        await self.make_transaction(channel,escrow_address,public_key,str(amount))        

    async def make_transaction(self,channel,escrow_address,public_key,amount):
        stub = pb2_grpc.NunetAdapterStub(channel)
        fp = pb2.AdapterReleaseFunds()
        fp.escrow_address = escrow_address
        fp.public_key=public_key
        fp.agi_amount=amount
        logger.info("Before making transaction")
        txn = await stub.makeAdapterTransaction(fp)
        logger.info("after making transaction")
        logger.info(txn)
        return txn.response 


    async def get_escrow(self,service,call_id,tracer,context,channel,tracer_info):
        cost_per_process = ""
        pubk = ""
        get_address=self.utils.get_address()
        address=get_address[0]
        port=get_address[1]
        nunet_adapter_address=address+":"+port

        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span(str(service)+"_escrow_span", context=ctx) as span:

            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            for service_definition in gather.service_definitions:
                if service == service_definition["name"] and "news-score" not in service:
                    #get machine provider's public key from the machine
                    stub = pb2_grpc.NunetAdapterStub(channel)
                    fp = pb2.AdapterMachineProviderInput()
                    fp.service_name = service
                    fp.tracer_info = str({
                        'trace_id': trace_id,
                        'span_id': span_id
                    })
                    logger.info("Getting provider payment params")
                    params = await stub.getProviderPaymentParams(fp)
                    pubk=params.pubk
                    cost_per_process=params.cost_per_process
                    break
                if "news-score" in service:
                    stub = nunet_adapter_pb2_grpc.NunetAdapterStub(channel)
                    fp = nunet_adapter_pb2.priceParams()
                    fp.service_name = service
                    fp.tracer_info = str({
                        'trace_id': trace_id,
                        'span_id': span_id
                    })
                    result = await stub.reqPrice(fp)
                    cost_per_process = result.cost_per_process
                    pubk = result.pubk
                    logger.info("Getting provider payment params of news-score")
                    logger.info(cost_per_process)
                    logger.info(pubk)

            logger.info("public key")
            logger.info(pubk)
            logger.info("cost per process")
            logger.info(cost_per_process)
            stub = pb2_grpc.NunetAdapterStub(channel)
            fp = pb2.PaymentParams()
            fp.cost_per_process = str(cost_per_process)
            fp.pubk = str(pubk)
            result= {"service":str(service),
                            "escrow_address":"",
                            "cost":str(cost_per_process),
                            "call_id":str(call_id)
                    }
            fp.call_id=str(result)
            def start_loop(loop):
                async def run():
                    logger.info("Signing adapter transaction")
                    await stub.signAdapterTransaction(fp)
                asyncio.set_event_loop(loop)
                
                loop.run_until_complete(run())

           
            if tokenomics_mode=="OFF":
                new_loop = asyncio.new_event_loop()
                t = threading.Thread(target=start_loop, args=(new_loop,))
                t.start()
                # new_loop.run_in_executor(executor,stub_tokenomics.signTransaction,fp)
                # create_escrow= threading.Thread(target=await stub.signAdapterTransaction, args=(fp,))
                # create_escrow.start()
            else:
                escrow_address=await stub.signAdapterTransaction(fp)
                result= {"service":service,
                            "escrow_address":escrow_address.escrow_address,
                            "cost":cost_per_process
                        }
            sleep(30 / 1000)
            span.add_event("event message", {"escrow address": str(result)})
        return result


    def calculate_global_probability(self,list_of_answers=[]):
        stance_classifiers = ["testing-uclnlp","uclnlp"]
        binary_classifiers = ["testing-binary-classification","binary_classification"]

        local_probability_stance = 0
        local_probability_binary = 0
        normalized_local_probability_stance = 0
        for return_value in list_of_answers:
            if return_value['name'] in stance_classifiers:
                c1, c2 = 1.0,1.0
                c3, c4 = -1.0,-1.0
                
                predictions = ast.literal_eval(return_value['return'])

                local_probability_stance = c1* predictions['disagree'] + c2* predictions['unrelated'] + c3 * predictions['agree'] + c4* predictions['discuss']

                normalized_local_probability_stance = (local_probability_stance + 1)/2
            if return_value['name'] in binary_classifiers:
                
                predictions = return_value['return']

                local_probability_binary = 1 if predictions == 1 else  0

            
        coef_stance = 0.2 # interpretation: stance explains 20% of variance of the global probability;
        coef_bc = 0.6 # interpretation: binary classification explains 60% of variance of the global probability;

        global_probability = coef_stance * normalized_local_probability_stance + coef_bc * local_probability_binary 

        return global_probability

    async def get_service_result(self,service,body,headline,call_id,escrow_addresses,tracer,context,channel,service_input,tracer_info):
        get_address=self.utils.get_address()
        address=get_address[0]
        port=get_address[1]
        nunet_adapter_address=address+":"+port
        fp = pb2.AdapterInput()

        fp.service_name = service
        fp.params = '{"body":"'+body+'","headline":"'+headline+'","call_id":"'+str(call_id)+'"}'
        fp.service_input_params=service_input
        public_key=""
        agi_amount=""
        for service_definition in gather.service_definitions:
            description = service_definition["description"]
            human_readable = service_definition["human_readable"]
            version = service_definition["version"]
            service_type = service_definition["type"]
            snet_service_id = service_definition["snet_service_id"]
            image = service_definition["image"]
            src_repo = service_definition["src_repo"]
            update_timestamp = service_definition["update_timestamp"]

            if service == service_definition["name"]:
                fp.declarations=json.dumps(service_definition["declarations"])
                public_key=service_definition["payment_parameters"]["public_key"]
                agi_amount=service_definition["payment_parameters"]["amount"]
                break

        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span(str(service)+"_result_span", context=ctx) as span:

            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id

            fp.tracer_info = str({
                'trace_id': trace_id,
                'span_id': span_id
            })

            stub = pb2_grpc.NunetAdapterStub(channel)
            result = await stub.callAdapter(fp)
            # result = next(result)
            logger.info(f"Call adapter result == {result}")
            result=eval(result.response)
            result=result["response"]
            resp=eval(result)
            result=resp[0]
            txn_hash=resp[1]
            telemetry=resp[2]
            escrow_address=""
            for itm in escrow_addresses:
                if itm["service"]==service:
                    escrow_address=itm #["escrow_address"]
            logger.info(str(escrow_addresses))
            await self.verify(channel,result,txn_hash,str(escrow_address),public_key,agi_amount)
        
        return {'name':service, 'human_readable':human_readable, 'type':service_type, 'version':version, 'snet_service_id':snet_service_id, 'src_repo':src_repo, 'image':image, 'update_timestamp':update_timestamp, 'description':description, 'return':result, 'telemetry':telemetry}

    async def get_result(self,headline,body,call_id,tracer,context,tracer_info): 
        metadata=json.load(open("metadata.json", "r"))

        ts = time.time()      

        get_address=self.utils.get_address()
        address=get_address[0]
        port=get_address[1]
        
        nunet_adapter_address=address+":"+port
        channel = grpc.aio.insecure_channel("{}".format(nunet_adapter_address))
        
        service_list=[]
        all_services=[]
        service_inputs=[]
        for service in metadata["services"]["services"]:
            service_list.append(service["name"])
            all_services.append(service["name"])
            service_inputs.append(service["declarations"]["service_input_params"])
        
        all_services.append(metadata["services"]["name"])

        
        average_result = {"agree": 0, 
                "disagree": 0,
                "discuss" : 0,
                "unrelated" : 0}
        escrow_addresses=[]

        
        async with grpc.aio.insecure_channel("{}".format(nunet_adapter_address)) as channel:
            i=0
            for service in all_services:
                escrow_addresses.append(await self.get_escrow(service,call_id,tracer,context,channel,tracer_info))
            cnt=0
            results=[]

            for service in service_list:
                results.append(await self.get_service_result(service,body,headline,call_id,escrow_addresses,tracer,context,channel,service_inputs[i],tracer_info))
                i+=1
        
        final_result = {}
        final_result['url'] = None
        final_result['timestamp']=ts
        final_result['result']={'general_probability': self.calculate_global_probability(results),'algorithms':results,"escrow_info":escrow_addresses}

        return {'call':final_result},escrow_addresses[-1]["escrow_address"], escrow_addresses[-1]


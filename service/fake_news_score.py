import sys
import grpc
import operator
import time
import os
import asyncio
from concurrent import futures
from asyncio import ensure_future, get_event_loop

from service.workflow import *
from service.gather import gather_metadata

sys.path.append("./service_spec")
sys.path.append("./service/service_spec")
import fake_news_score_pb2 as pb2
import fake_news_score_pb2_grpc as pb2_grpc

import service_proto_pb2
import service_proto_pb2_grpc 


from resutils import *
import logging
import log

import nunet_adapter_pb2
import nunet_adapter_pb2_grpc


from run_fake_news_score import Log
import json


from time import sleep

import threading


work_flow=workflow()

import opentelemetry
from opentelemetry import trace
from opentelemetry.instrumentation.grpc import GrpcInstrumentorServer
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.jaeger.thrift import JaegerExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import set_span_in_context


jaeger_address = os.environ['jaeger_address']


jaeger_exporter = JaegerExporter(
    agent_host_name=jaeger_address,
    agent_port=6831,
)


trace_provider=trace.get_tracer_provider().add_span_processor(
    BatchSpanProcessor(jaeger_exporter)
)

grpc_server_instrumentor = GrpcInstrumentorServer()
grpc_server_instrumentor.instrument()

tracer_server=opentelemetry.instrumentation.grpc.server_interceptor(tracer_provider=trace_provider)
tracer_grpc=trace.get_tracer(tracer_server)

tracer=trace.get_tracer(__name__)

logger=Log.logger


try:
    grpc_port = os.getenv("NOMAD_PORT_rpc")
except:
    grpc_port = "7009" 

try:
    deployment_type=os.environ['deployment_type']
except:
    deployment_type=""

try:
    tokenomics_mode=os.environ['tokenomics_mode']
except:
    tokenomics_mode=""

class GRPCfns(pb2_grpc.FakeNewsScoreServicer):


    async def fn_score_calc(self, req, ctxt):
        current_span = trace.get_current_span()
        current_span.set_attribute("http.route", "some_route")
        
        sleep(30 / 1000)

        tracer_info = req.tracer_info
        tracer_info = eval(tracer_info)
        span_id = tracer_info['span_id']
        trace_id = tracer_info['trace_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))

        with tracer.start_as_current_span("fn_score_calc", context=ctx) as span:

            trace_info=span.get_span_context()
            span_id = trace_info.span_id
            trace_id = trace_info.trace_id
            tracer_info = {
                'trace_id': trace_id,
                'span_id': span_id
            }
            try:
                telemetry=resutils()
                start_time=time.time()
                cpu_start_time=telemetry.cpu_ticks()
            except Exception as e:
                exception = Exception(str(e))
                span.record_exception(exception)
                span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)
            
            headline = req.headline
            body = req.body
            call_id=req.call_id
            

            logger.info(tracer_info)

            #TODO these seem to duplicate the pre-processing function in rest-api/rest-api/utils.py
            # if not, all pre-processing should be moved there.
            body=body.replace("\'","\\\'").replace("\"","\\\"")
            body=body.replace("\n"," ").replace("\t"," ")
            body=body.replace("\b"," ").replace("\r"," ")
            body=body.replace("\f"," ")
                                
            headline=headline.replace("\'","\\\'").replace("\"","\\\"")
            headline=headline.replace("\n"," ").replace("\t"," ")
            headline=headline.replace("\b"," ").replace("\r"," ")
            headline=headline.replace("\f"," ")
            fn_score = pb2.Score()
            resp=pb2.Resp()
            

            get_address=telemetry.get_address()
            address=get_address[0]
            port=get_address[1]
            
            nunet_adapter_address=address+":"+port
            sleep(30 / 1000)
            span.add_event("event message", {"nunet adapter address": str(nunet_adapter_address)})
                    
            context = set_span_in_context(current_span)

            final_result,escrow_address,escrow_info = await work_flow.get_result(headline,body,call_id,tracer,context,tracer_info)
            sleep(30 / 1000)
            span.add_event("event message", {"dependecies result": str(final_result)})

            #print(type(final_result))
            # extract the data
            call = final_result['call']
            result = call['result']
            algorithms = result['algorithms']
            
            gnrl_prob = result['general_probability']
            ret = {'general_probability':gnrl_prob}

            try:
                memory_used=telemetry.memory_usage()
                time_taken=time.time()-start_time
                cpu_used=telemetry.cpu_ticks()-cpu_start_time
                net_used=telemetry.block_in()

                if deployment_type=="prod":
                    with open('service/service_spec/service_definition_prod.json', 'r') as file:
                        service_definition_str = file.read()        
                else:
                    with open('service/service_spec/service_definition.json', 'r') as file:
                        service_definition_str = file.read()
                
                service_definition = json.loads(service_definition_str)
                serv_name = service_definition["name"]
                human_readable = service_definition["human_readable"]
                version = service_definition["version"]
                service_type = service_definition["type"]
                snet_service_id = service_definition["snet_service_id"]
                image = service_definition["image"]
                src_repo = service_definition["src_repo"]
                description = service_definition["description"]
                update_timestamp = service_definition["update_timestamp"]
                agi_amount=service_definition["payment_parameters"]["amount"]
                public_key=service_definition["payment_parameters"]["public_key"]

                telm = {
                    "memory used":memory_used, 
                    "cpu used":cpu_used, 
                    "network used":net_used, 
                    "time_taken":time_taken
                    }
                
                algorithms.append({
                    "name":serv_name, 
                    "human_readable":human_readable, 
                    "version":version, 
                    "type":service_type, 
                    "snet_service_id":snet_service_id, 
                    "image":image, 
                    "src_repo":src_repo, 
                    "description":description, 
                    "update_timestamp":update_timestamp, 
                    "return":ret, 
                    "telemetry":telm
                    })
                
                # update the final result with the updated data
                result['algorithms'] = algorithms
                call['result'] = result
                final_result['call'] = call
                print("final result", final_result)
                response=""
                resp.response=str(final_result)

                channel = grpc.insecure_channel("{}".format(nunet_adapter_address)) 
                txn_hash=await telemetry.call_telemetry(str(resp.response),cpu_used,memory_used,net_used,time_taken,nunet_adapter_address,call_id,tracer_info)   
                response=[str(resp.response),str(txn_hash)]
                
                response=str(response)
                logger.info(escrow_info)
                async with grpc.aio.insecure_channel("{}".format(nunet_adapter_address)) as channel:
                    tx = await work_flow.make_transaction(channel,str(escrow_info),public_key,str(agi_amount/10**4))
                logger.info(response)
            except Exception as e:
                exception = Exception(str(e))
                span.record_exception(exception)
                span.set_status(Status(StatusCode.ERROR, "error happened"))
                logger.error(e)
            sleep(30 / 1000)
            span.add_event("event message", {"news-score result": str(resp)})
            logger.info(resp)
            return resp

class GRPCproto(service_proto_pb2_grpc.ProtoDefnitionServicer):                      

    async def req_msg(self, req, ctxt):
        #TODO:  use https://googleapis.dev/python/protobuf/latest/ instead of reading from file 
        with open('service/service_spec/fake_news_score.proto', 'r') as file:
            proto_str = file.read()
        reqMessage=service_proto_pb2.reqMessage()
        reqMessage.proto_defnition=proto_str
        reqMessage.service_stub="FakeNewsScoreStub"
        reqMessage.service_input="InputFNS"
        reqMessage.function_name="fn_score_calc"
        reqMessage.service_input_params='["body","headline","call_id","tracer_info"]'
        return reqMessage

    async def req_metadata(self, req, ctxt):
        #TODO:  use https://googleapis.dev/python/protobuf/latest/ instead of reading from file 
        with open('service/service_spec/fake_news_score.proto', 'r') as file:
            proto_str = file.read()
        
        service_name=req.service_name
        tracer_info = eval(req.tracer_info)
        trace_id = tracer_info['trace_id']
        span_id = tracer_info['span_id']

        span_context = SpanContext(
        trace_id=trace_id,
        span_id=span_id,
        is_remote=True,
        trace_flags=TraceFlags(0x01)
        )
        ctx = trace.set_span_in_context(NonRecordingSpan(span_context))
        with tracer.start_as_current_span("req_metadata", context=ctx) as span:
        
            respMetadata=service_proto_pb2.respMetadata()
            
            proto_defnition=proto_str
            service_stub="FakeNewsScoreStub"
            service_input="InputFNS"
            function_name="fn_score_calc"
            service_input_params='["body","headline","call_id"]'
            try:
                deployment_type=os.environ['deployment_type']
            except:
                deployment_type=""
            
            if deployment_type=="prod":
                with open('service/service_spec/service_definition_prod.json', 'r') as file:
                    service_definition_str = file.read()        
            else:
                with open('service/service_spec/service_definition.json', 'r') as file:
                    service_definition_str = file.read()

            service_definition=json.loads(service_definition_str)
            service_definition["declarations"]["protobuf_definition"]=proto_defnition
            service_definition["declarations"]["service_stub"]=service_stub
            service_definition["declarations"]["function"]=function_name        
            service_definition["declarations"]["input"]=service_input
            service_definition["declarations"]["service_input_params"]=service_input_params

            respMetadata.service_definition=json.dumps(service_definition)
            return respMetadata


async def main():
    # grpc_server = grpc.aio.server(migration_thread_pool=futures.ThreadPoolExecutor(max_workers=1000))
    grpc_server = grpc.aio.server()
    pb2_grpc.add_FakeNewsScoreServicer_to_server(GRPCfns(), grpc_server)
    service_proto_pb2_grpc.add_ProtoDefnitionServicer_to_server(GRPCproto(), grpc_server)
    grpc_server.add_insecure_port('[::]:' + str(grpc_port))
    
    logger.info("GRPC Server Started on port: " + str(grpc_port))
    await grpc_server.start()
    await grpc_server.wait_for_termination()	




if __name__ == "__main__":
    # grpc_server = grpc.server(futures.ThreadPoolExecutor(max_workers=1000))
    logger.info("Running fake news score file")
    tasks = [asyncio.ensure_future(gather_metadata()),
             asyncio.ensure_future(main())]
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(*tasks))
    
    


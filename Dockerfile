FROM ubuntu:18.04

RUN apt-get update && apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        build-essential \
        python3.6 \
        python3.6-dev \
        python3-pip \
        python-setuptools \
        cmake \
        wget \
        curl \
        libsm6 \
        libxext6 \ 
        libxrender-dev \
        vim \
        lsof

RUN SNETD_GIT_VERSION=`curl -s https://api.github.com/repos/singnet/snet-daemon/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")' || echo "v3.1.6"` && \
    SNETD_VERSION=${snetd_version:-${SNETD_GIT_VERSION}} && \
    cd /tmp && \
    wget https://github.com/singnet/snet-daemon/releases/download/${SNETD_VERSION}/snet-daemon-${SNETD_VERSION}-linux-amd64.tar.gz && \
    tar -xvf snet-daemon-${SNETD_VERSION}-linux-amd64.tar.gz && \
    mv snet-daemon-${SNETD_VERSION}-linux-amd64/snetd /usr/bin/snetd && \
    rm -rf snet-daemon-*


RUN python3.6 -m pip install -U pip
RUN python3.6 -m pip install --upgrade setuptools


ENV SINGNET_REPOS=/opt/singnet

ARG nunet_adapter_uclnlp_address
ARG nunet_adapter_athene_address
ARG nunet_adapter_address

ENV nunet_adapter_name=${nunet_adapter_name}
ENV nunet_adapter_uclnlp_name=${nunet_adapter_uclnlp_name}
ENV nunet_adapter_athene_name=${nunet_adapter_athene_name}


RUN wget https://github.com/fullstorydev/grpcurl/releases/download/v1.1.0/grpcurl_1.1.0_linux_x86_64.tar.gz
RUN tar -xvzf grpcurl_1.1.0_linux_x86_64.tar.gz
RUN chmod +x grpcurl
RUN mv grpcurl /usr/local/bin/grpcurl

ENV LS_SERVICE_NAME=news-score
ENV LS_ACCESS_TOKEN=ej4S2fSomE5V7uP/LYJLw9frBRnBRqHVoQiRYSCwoSld1MncIAHByOSkc8jcKiSAbhVEy2zI0Emjw38vVF8c87vFd6Q1wMCnI/DD4/Bi
ENV OTEL_PYTHON_TRACER_PROVIDER=sdk_tracer_provider
ENV jaeger_address=testserver.nunet.io

RUN python3.6 -m pip install opentelemetry-launcher
RUN opentelemetry-bootstrap -a install



RUN python3.6 -m pip install opentelemetry-exporter-jaeger==1.2.0
RUN python3.6 -m pip install opentelemetry-api==1.3.0
RUN python3.6 -m pip install opentelemetry-sdk==1.3.0

COPY . /${SINGNET_REPOS}/fns
WORKDIR /${SINGNET_REPOS}/fns

RUN python3.6 -m pip install -r requirements.txt

RUN sh buildproto.sh

ENV SERVICE_PORT=7031
ENV NOMAD_PORT_rpc=7031
ENV deployment_type=prod
ENV tokenomics_mode=OFF

EXPOSE 7030
EXPOSE 7031


CMD ["python3.6", "run_fake_news_score.py", "--daemon-config", "snetd.config.json"]
#CMD ["python3.6", "run_fake_news_score.py", "--no-daemon"]